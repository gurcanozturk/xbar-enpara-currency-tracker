#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
# <xbar.title>Enpara Currency Tracker</xbar.title>
# <xbar.version>1.0</xbar.version>
# <xbar.author>A.Gurcan Ozturk</xbar.author>
# <xbar.author.github>gurcanozturk</xbar.author.github>
# <xbar.desc>Shows USD/TRY rate from Enpara web site</xbar.desc>
# <xbar.dependencies>python</xbar.dependencies>
# <xbar.image></xbar.image>

import bs4
import re
import requests

url = "https://www.qnbfinansbank.enpara.com/hesaplar/doviz-ve-altin-kurlari"
content = requests.get(url).text

soup = bs4.BeautifulSoup(content,features="html.parser")

def show_currency(currency):
  if currency in divs.text and 'Parite' not in divs.text:
    curr = (divs.find_all('span')[1].text)
    result = re.findall(r"[-+]?\d*\,\d+|\d+", curr)
    alis   = (result[0][:6])
    print ("%s: %s" % (currency, alis))
    print ("---")

for divs in soup.find_all('div', class_ = 'enpara-gold-exchange-rates__table-item'):
  show_currency('USD')
  show_currency('EUR')